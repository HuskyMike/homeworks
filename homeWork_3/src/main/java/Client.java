import java.util.Comparator;


public class Client implements Comparable<Client> {

    private String name;

    private int score;


    public Client(String name, int score) {

        this.name = name;

        this.score = score;

    }


    public String getName() {

        return name;

    }


    public void setName(String name) {

        this.name = name;

    }


    public int getScore() {

        return score;

    }


    public void setScore(int score) {

        this.score = score;

    }


    public int compareTo(Client c) {

        return name.compareTo(c.getName());

    }


}