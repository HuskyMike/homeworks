import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {
    public static void main(String[] args) {
        // Переопределение
        Client client1 = new Client("Леха", 500);
        Client client2 = new Client("Миша", 1000);
        Transfer transfer = new Transfer(client1, client2, 100);
        Credit credit = new Credit("Ипотека", 5000);



        System.out.println(client1.toString());
        System.out.println(client2.toString());
        System.out.println(credit.toString());
        System.out.println(transfer.toString());


        //--------------------------------------------------------------------
        CreditServiceImpl service = new CreditServiceImpl();
        service.extinguished(client1,credit);
        service.receivingCredit(client2,credit);
    }
}
