public class CreditServiceImpl implements CreditService {

    @Override
    public void receivingCredit(Client client, Credit credit) {
        System.out.println("Кредит " + credit.getNameCredit() + " клиенту " + client.getName() + " одобрен");
    }

    @Override
    public void extinguished(Client client, Credit credit) {
        System.out.println("Кредит " + credit.getNameCredit() +" у клиента " + client.getName() + " погашен");
    }

}
