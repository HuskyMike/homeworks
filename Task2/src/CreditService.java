public interface CreditService {

    void receivingCredit(Client client,Credit credit);

    void extinguished(Client client,Credit credit);
}
