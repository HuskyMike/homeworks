import java.util.Objects;

public class Transfer {
    private Client client;
    private String sanderName;
    private String recipientName;
    private int summ;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return summ == transfer.summ &&
                Objects.equals(client, transfer.client) &&
                Objects.equals(sanderName, transfer.sanderName) &&
                Objects.equals(recipientName, transfer.recipientName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(client, sanderName, recipientName, summ);
    }

    @Override
    public String toString() {
        String cl = String.valueOf(Transfer.class);
        String clas = null;
        for(String word : cl.split(" ")){
            clas = word;
        }
        return "{" + "\"сlass\" \"" + clas + "\"," +
                "\"sanderName\":\"" + sanderName + "\"," +
                "\"recipientName\":\"" + recipientName + "\"," +
                "\"summ\":\"" + summ + "\"" +
                "}";
    }

    Transfer(Client sender, Client recipient, int summ) {
        this.sanderName = sender.getName();
        this.recipientName = recipient.getName();
        this.summ = summ;
        sender.setScore(sender.getScore() - summ);
        recipient.setScore(recipient.getScore() + summ);
    }
}
