import java.util.Objects;

public class Client {
    private String name;
    private int score;

    public Client(String name, int score) {
        this.name = name;
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return score == client.score &&
                name.equals(client.name);
    }

    @Override
    public String toString() {
        String cl = String.valueOf(Client.class);
        String clas = null;
        for(String word : cl.split(" ")){
            clas = word;
        }
        return "{" + "\"сlass\" \"" + clas + "\"," +
                "\"name\":\"" + name + "\"," +
                "\"score\":\"" + score + "\"" +"}" ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, score);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
