import java.util.Objects;

public class PinValidator {
    private Integer pinCode;

    @Override
    public String toString() {
        return "PinValidator{" +
                "pinCode=" + pinCode +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PinValidator that = (PinValidator) o;
        return Objects.equals(pinCode, that.pinCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pinCode);
    }

    public PinValidator(Integer pinCode) {
        this.pinCode = pinCode;
    }

    public Integer getPinCode() {
        return pinCode;
    }

}
