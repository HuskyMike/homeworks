import java.util.Objects;

public class TerminalServer {
    private boolean connection = Math.random() < 0.5;
    private final PinValidator pinCodeServer;
    private Integer balance;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TerminalServer server = (TerminalServer) o;
        return connection == server.connection &&
                Objects.equals(pinCodeServer, server.pinCodeServer) &&
                Objects.equals(balance, server.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connection, pinCodeServer, balance);
    }

    @Override
    public String toString() {
        return "TerminalServer{" +
                "connection=" + connection +
                ", pinCodeServer=" + pinCodeServer +
                ", balance=" + balance +
                '}';
    }

    public TerminalServer(PinValidator pinCodeServer, Integer balance) {
        this.pinCodeServer = pinCodeServer;
        this.balance = balance;
    }


    //Пополнение баланса
    public void addScore(Integer sum) {
        this.balance = this.balance + sum;
        System.out.println("Печатает чек");
        System.out.println("*На чеке* Сумма: " + balance);
    }

    //Снятия с счета
    public void deleteWithScore(Integer sum) throws Exception {
        if (sum <= balance) {
            balance = balance - sum;
            System.out.println("Печатает чек");
            System.out.println("*На чеке* Сумма: " + balance);
        } else {
            throw  new Exception("Недостаточно средств");

        }
    }


    public PinValidator getPinCodeServer() {
        return pinCodeServer;
    }

    //Проверка балланса
    public Integer getBalance() {
        return balance;
    }

    public boolean isConnection() {
        return connection;
    }
}