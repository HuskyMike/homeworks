import static org.junit.Assert.*;

public class TerminalServerTest {
    PinValidator pinValidator = new PinValidator(4243);
    TerminalServer terminalServer = new TerminalServer(pinValidator,500);

    @org.junit.Test
    public void addScore() {
        terminalServer.addScore(500);
    }

    @org.junit.Test
    public void deleteWithScore() throws Exception {
        terminalServer.deleteWithScore(500);
    }

    @org.junit.Test
    public void getBalance() {
        System.out.println(terminalServer.getBalance());
    }
}