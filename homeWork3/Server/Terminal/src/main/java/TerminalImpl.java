import javax.security.auth.login.AccountLockedException;
import java.util.Objects;

public class TerminalImpl implements Terminal {
    private final TerminalServer server;
    private Integer pinCodeUser;
    private int attempts;
    private int status;
    private String problemConnection = "Проблемы с соединением, не приходите больше";
    private String pinCodeError = "Неправильный пин код, Вспоминай. " +
            "Количество попыток: ";
    private String attemptError = "Превышенно количество попыток. Блокировка на 5 секунд";
    private String block = "Блокировка";


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TerminalImpl terminal = (TerminalImpl) o;
        return attempts == terminal.attempts &&
                status == terminal.status &&
                Objects.equals(server, terminal.server) &&
                Objects.equals(pinCodeUser, terminal.pinCodeUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(server, pinCodeUser, attempts, status);
    }

    @Override
    public String toString() {
        return "TerminalImpl{" +
                "server=" + server +
                ", pinCodeUser=" + pinCodeUser +
                ", attempts=" + attempts +
                ", status=" + status +
                '}';
    }

    public TerminalImpl(TerminalServer server, Integer pinCodeUser) {
        this.server = server;
        this.pinCodeUser = pinCodeUser;
    }

    private void checkOk() throws Exception {
        if (this.server.isConnection() == false) {
            throw new Exception(problemConnection);
        }
        if (!server.getPinCodeServer().getPinCode().equals(getPinCodeUser())) {
            attempts++;
            throw new Exception(pinCodeError + (4 - attempts));
        }
        if (attempts == 2) {
            throw new AccountLockedException(attemptError);
        }
        if (attempts == 2){
            Thread.sleep(5000);
        }
        if (server.getPinCodeServer().getPinCode().equals(getPinCodeUser()) && server.isConnection() == true) {
            status = 1;
        } else {
            status = 0;
        }

    }


    public void replayPinCode(Integer newPinCodeUser) throws InterruptedException {
        if (attempts == 3) {
            System.out.println(new AccountLockedException());
            Thread.sleep(5000);
            attempts = 0;
        }
        this.pinCodeUser = newPinCodeUser;
        attempts++;
    }

    @Override
    public void balance() throws Exception {
        checkOk();
        if (status == 1) {
            System.out.println("Баланс: " + server.getBalance());
        }
    }

    @Override
    public void plusBalance(Integer sum) throws Exception {
        checkOk();
        if (status == 1) {
            server.addScore(sum);
        }
    }

    @Override
    public void minusBalance(Integer sum) throws Exception {
        checkOk();
        if (status == 1) {
            server.deleteWithScore(sum);
        }
    }

    public Integer getPinCodeUser() {
        return pinCodeUser;
    }

    public void setProblemConnection(String problemConnection) {
        this.problemConnection = problemConnection;
    }

    public void setPinCodeError(String pinCodeError) {
        this.pinCodeError = pinCodeError;
    }

    public void setAttemptError(String attemptError) {
        this.attemptError = attemptError;
    }

    public void setBlock(String block) {
        this.block = block;
    }
}
