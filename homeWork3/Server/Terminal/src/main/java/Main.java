public class Main {
    public static void main(String[] args) throws Exception {
        PinValidator pinValidator = new PinValidator(4243);
        TerminalServer server = new TerminalServer(pinValidator,500);
        TerminalImpl terminal = new TerminalImpl(server,4243);

        System.out.println(server.isConnection());
        terminal.balance();
        terminal.plusBalance(500);
        terminal.minusBalance(300);

    }
}
