public interface Terminal {

    //Проверка балланса
    void balance() throws Exception;

    //Пополнение балланса
    void plusBalance(Integer sum) throws Exception;

    //Снятие с балланса
    void minusBalance(Integer sum) throws Exception;
}
