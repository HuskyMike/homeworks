import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TerminalImplTest {

    PinValidator pinValidator = new PinValidator(4243);
    TerminalServer server = new TerminalServer(pinValidator, 500);
    TerminalImpl terminal = new TerminalImpl(server, 4243);
    Integer newPinCodeUser = 4243;


    @Test
    public void testReplayPinCode() throws InterruptedException {
        terminal.replayPinCode(newPinCodeUser);
        assertEquals(pinValidator.getPinCode(),newPinCodeUser);
    }

    @Test
    public void testBalance() throws Exception {
        terminal.balance();
    }

    @Test
    public void TestPlusBalance() throws Exception {
        terminal.plusBalance(500);
    }

    @Test
    public void testMinusBalance() throws Exception {
        terminal.minusBalance(500);
    }
}