package homeWork_11_1;

public class Foo {
    final Object lock = new Object();

    public Foo() {
    }


    public void first(Runnable printFirst) throws InterruptedException {
        printFirst.run();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        Thread.sleep(30);
        printSecond.run();

    }

    public void third(Runnable printThird) throws InterruptedException {
        Thread.sleep(50);
        printThird.run();
    }


}
