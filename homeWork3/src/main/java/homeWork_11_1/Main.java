package homeWork_11_1;

public class Main {
    private static Runnable first;
    private static Runnable second;
    private static Runnable third;

    public static void main(String[] args) throws InterruptedException {
        final Foo foo = new Foo();


        first = new Runnable() {
            public void run() {
                System.out.print("first");
            }
        };

        second = new Runnable() {
            public void run() {
                System.out.print("second");
            }
        };

        third = new Runnable() {
            public void run() {
                System.out.print("third");
            }
        };


        Thread a = new Thread(new Runnable() {
            public void run() {
                try {
                    foo.first(first);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A");

        Thread b = new Thread(new Runnable() {
            public void run() {
                try {
                    foo.second(second);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B");

        Thread c = new Thread(new Runnable() {
            public void run() {
                try {
                    foo.third(third);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "С");


        a.start();
        b.start();
        c.start();

    }


}
