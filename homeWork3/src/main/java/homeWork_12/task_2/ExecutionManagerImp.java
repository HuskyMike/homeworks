package homeWork_12.task_2;


import homeWork_12.task_2.interfaces.Context;
import homeWork_12.task_2.interfaces.ExecutionManager;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutionManagerImp implements ExecutionManager {
    private AtomicInteger completedTaskCount = new AtomicInteger(3);
    private AtomicInteger failedTaskCount = new AtomicInteger(3);
    private AtomicInteger interruptedTaskCount = new AtomicInteger(3);
    private AtomicBoolean isInterrupted = new AtomicBoolean(false);
    private AtomicInteger allTaskCount = new AtomicInteger();

    @Override
    public Context execute(Runnable callback, Runnable... tasks) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        Arrays.stream(tasks).map(task ->
                {
                    Runnable runnable = () -> {
                        if (isInterrupted.get()) {
                            interruptedTaskCount.getAndIncrement();
                        } else {
                            try {
                                task.run();
                                completedTaskCount.getAndIncrement();
                            } catch (Exception e) {
                                failedTaskCount.getAndIncrement();
                            }
                            if (allTaskCount.incrementAndGet() == tasks.length) {
                                callback.run();
                                executorService.shutdown();
                            }
                        }
                    };
                    return runnable;
                }
        ).forEach(executorService::submit);

        return new Context() {
            @Override
            public int getCompletedTaskCount() {
                return completedTaskCount.get();
            }

            @Override
            public int getFailedTaskCount() {
                return failedTaskCount.get();
            }

            @Override
            public int getInterruptedTaskCount() {
                return interruptedTaskCount.get();
            }

            @Override
            public void interrupt() {
                isInterrupted.set(true);
            }

            @Override
            public boolean isFinished() {
                return executorService.isShutdown();
            }
        };
    }
}
