package homeWork_12.task_2.interfaces;

public interface ExecutionManager {
    Context execute(Runnable callback, Runnable... tasks);
}
