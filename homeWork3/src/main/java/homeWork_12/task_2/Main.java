package homeWork_12.task_2;

import homeWork_12.task_2.interfaces.ExecutionManager;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ExecutionManager executionManager = new ExecutionManagerImp();

        Runnable runnable = () -> System.out.println(Thread.currentThread().getName());

        Runnable[] tasks = new Runnable[5];
        Arrays.fill(tasks, runnable);

        executionManager.execute(() -> System.out.println("Устал"), tasks);

    }
}
