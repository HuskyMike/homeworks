package homeWork_12.task_1;

import java.util.concurrent.Callable;

public class Main {
    public static void main(String[] args) throws Exception {

        Callable callable1 = (() -> {
            System.out.println(Thread.currentThread().getName());
            return null;
        });

        Callable callable2 = (() -> {
            System.out.println(Thread.currentThread().getName());
            return 30;
        });

        Task<Integer> task = new Task<>(callable1);
        Task<Integer> task1 = new Task<>(callable2);

        Thread thread1 = new Thread(() -> {
            try {
                System.out.println(task1.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "А");

        Thread thread2 = new Thread(() -> {
            try {
                System.out.println(task1.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "Б");

        thread1.start();
        thread2.start();


    }
}
