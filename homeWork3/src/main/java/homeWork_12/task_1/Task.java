package homeWork_12.task_1;

import java.util.concurrent.Callable;

public class Task<T> {
    private Callable<? extends T> callable;
    private volatile T result;
    private volatile boolean enterException = false;

    public Task(Callable<? extends T> callable) {
        this.callable = callable;
    }

    public T get() throws Exception {
        if (result == null) {
            synchronized (this) {
                if (result == null) {
                    try {
                        result = callable.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new Exception("My error");
                    }
                }
                if (enterException) {
                    throw new Exception("My error");
                }
            }
        }
        return result;
    }

}
