package homeWork_6;

import homeWork_6.annotations.*;
import homeWork_6.validation.Validator;

import java.util.Date;
import java.util.Objects;

public class Person {

    // не должно быть пустым, не более 32 символов
    @NotEmpty
    @MaxLength
    private String name;
    // не должно быть пустым, не более 32 символов
    @NotEmpty
    @MaxLength
    private String surname;
    // может быть пустым, не более 32 символов
    @MaxLength
    private String middleName;
    //Не должно быть пустым
    @NotNull()
    private Date birthdayDate;
    // только положительные значения
    @Min
    @Max
    private int age;
    // не менее 6 символов
    @MinLength
    private String password;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthdayDate=" + birthdayDate +
                ", age=" + age +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(middleName, person.middleName) &&
                Objects.equals(birthdayDate, person.birthdayDate) &&
                Objects.equals(password, person.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, middleName, birthdayDate, age, password);
    }

    public Person(String name, String surname, String middleName, Date birthdayDate, int age, String password) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.birthdayDate = birthdayDate;
        this.age = age;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public int getAge() {
        return age;
    }

    public String getPassword() {
        return password;
    }
}