package homeWork_6.validation;

public interface Validator<T> {


    // это JavaDoc. Есть плагины, которые автоматически добавляют их в документацию, формирую HTML-страницу.
    // Пример JavaDoc'a https://docs.spring.io/spring-framework/docs/current/javadoc-api/ (просто для понимания, как он выглядит)
    /**
     * Метод проверяет значения полей объекта, помеченные соответствующими аннотациями
     * @param obj объект, значения полей которого необходимо проверить
     * @throws IllegalArgumentException если поле не прошло проверку, метод выбросит IllegalArgumentException
     */
    void validate(T obj) throws IllegalArgumentException, IllegalAccessException;
}
