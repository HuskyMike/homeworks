package homeWork_6;

import homeWork_6.annotations.*;
import homeWork_6.validation.Validator;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) throws IllegalAccessException {
        final Calendar calendar = new GregorianCalendar(2005, Calendar.NOVEMBER, 26);
        Date date = calendar.getTime();
        Person person = new Person("Акакий", "Иванов", "Иванович", date, 15, "1234");
        System.out.println(person);


        //Для NotNull
        Validator validatorNotNull = new Validator() {
            @Override
            public void validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
                Class<?> clazz = obj.getClass();
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(NotNull.class)) {
                        if (field.get(obj).equals(null)) {
                            throw new IllegalArgumentException();
                        }
                    }
                }
            }
        };
        //Для NotEmpty
        Validator validatorNotEmpty = new Validator() {
            @Override
            public void validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
                Class<?> clazz = obj.getClass();
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(NotEmpty.class)) {
                        if (field.get(obj).toString().equals("")) {
                            throw new IllegalArgumentException();
                        }
                    }
                }
            }

        };

        //Для Min
        Validator validatorMin = new Validator() {
            @Override
            public void validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
                Class<?> clazz = obj.getClass();
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(Min.class)) {
                        Min min = field.getAnnotation(Min.class);
                        int minAge = min.value();

                        int age = field.getInt(obj);
                        if (age <= minAge) {
                            throw new IllegalStateException(field.getName() + " возраст должен быть положительным" +
                                    " и больше 0.");
                        }
                    }
                }
            }
        };

        //Для Max
        Validator validatorMax = new Validator() {
            @Override
            public void validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
                Class<?> clazz = obj.getClass();
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(Max.class)) {
                        Max max = field.getAnnotation(Max.class);
                        int maxAge = max.value();

                        int age = field.getInt(obj);
                        if (age >= maxAge){
                            throw new IllegalStateException(field.getName() + " значение не может превышать 150" +
                                    " (или вы робот?!).");
                        }
                    }
                }
            }
        };

        //Для MinLength
        Validator validatorMinLength = new Validator() {
            @Override
            public void validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
                Class<?> clazz = obj.getClass();
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(MinLength.class)) {
                        MinLength min = field.getAnnotation(MinLength.class);
                        int minStr = min.value();

                        String str = field.get(obj).toString();
                        if (str.length() < minStr) {
                            throw new IllegalStateException(field.getName() + " значение не может быть пустым.");
                        }
                    }
                }
            }
        };

        //Для MaxLength
        Validator validatorMaxLength = new Validator() {
            @Override
            public void validate(Object obj) throws IllegalArgumentException, IllegalAccessException {
                Class<?> clazz = obj.getClass();
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(MaxLength.class)) {
                        MaxLength max = field.getAnnotation(MaxLength.class);
                        int maxStr = max.value();

                        String str = field.get(obj).toString();
                        if (str.length() > maxStr){
                            throw new IllegalStateException(field.getName() + " значение не может превышать 32 символа.");
                        }
                    }
                }
            }
        };
        validatorNotEmpty.validate(person);
    }
}

