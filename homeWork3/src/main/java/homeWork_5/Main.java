package homeWork_5;

public class Main {
    public static void main(String[] args) throws Exception {
        PinValidator pinValidator = new PinValidator(4243);
        TerminalServer server = new TerminalServer(pinValidator,500);
        TerminalImpl terminal = new TerminalImpl(server,4243);

        System.out.println(server.isConnection());
        terminal.balance();
        terminal.replayPinCode(4343);
        terminal.plusBalance(500);
        terminal.balance();
//        terminal.replayPinCode(4243);
        terminal.minusBalance(300);
        terminal.balance();


    }
}
