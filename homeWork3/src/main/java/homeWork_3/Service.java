package homeWork_3;

import java.util.*;


public class Service {

    private Client client;

    private Credit credit;

    private TreeMap<Client, TreeSet<Credit>> listClients;


    // Клиенту добавляется кредит

    void createCredit(Client client, Credit credit) {

        TreeSet<Credit> listCredits = new TreeSet<Credit>();

        if (listClients != null) {

            if (listClients.get(client) != null) {

                for (Credit value : listClients.get(client)) {

                    listCredits.add(value);

                }

            }

        } else {

            listClients = new TreeMap<Client, TreeSet<Credit>>();

        }

        listCredits.add(credit);

        listClients.put(client, listCredits);

    }


    //Подсчет кредитов

    int summAllCredits(Client client) {

        int number = 0;

        for (TreeSet<Credit> value : listClients.values()) {

            number = value.size();

        }

        return number;

    }


    /*

    Сортировать клиентов и их кредиты:

    1) только по клиенту (например, по ФИО)

    */

    TreeMap<Client, TreeSet<Credit>> filterClientName() {

        TreeSet<Client> clients = new TreeSet<Client>();

        for (Client obj : listClients.keySet()) {

            clients.add(obj);

        }

        return listClients;

    }


    /*

    Сортировать клиентов и их кредиты:

    2) только по сумме кредитов

    */

    TreeMap<Client, TreeSet<Credit>> filterCreditAmount() {

        List<Client> listFilter = new ArrayList<Client>();

        TreeSet<Credit> listCredit;


        for (Client object : listClients.keySet()) {

            listFilter.add(object);

        }


        for (Client name : listFilter) {

            listCredit = listClients.get(name);

            List<Credit> list = new ArrayList<Credit>();

            list.addAll(listCredit);

            Collections.sort(list);

            listCredit.addAll(list);

            listClients.remove(name);

            listClients.put(name, listCredit);

        }


        return listClients;

    }


    /*

Сортировать клиентов и их кредиты:

3) по клиенту и сумме кредитов, где сортировка по ФИО приоритетнее.

*/

    TreeMap<Client, TreeSet<Credit>> filterrFioCredit() {
        return listClients;

    }

}

