package homeWork_3;

public class Credit implements Comparable<Credit> {

    private String name;

    private int amount;


    public Credit(String name, int amount) {

        this.name = name;

        this.amount = amount;

    }


    public String getName() {

        return name;

    }


    public int getAmount() {

        return amount;

    }


    public int compareTo(Credit o) {

        return getAmount() - o.getAmount();

    }

}