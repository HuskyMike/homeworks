package homeWork_10.src.main.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Акакий", 30);
        Person person2 = new Person("Ококий", 19);
        List<Person> testList = new ArrayList<>();
        testList.add(person1);
        testList.add(person2);


        Map<String, Person> map = Streams.of(testList)
                .filter(p -> p.getAge() > 10)
                .transform(p -> new Person(p.getName(), p.getAge() + 30))
                .filter(person -> person.getAge() > 59)
                .transform(p -> p.getName() + " " + p.getAge())
                .transform(p -> new Person(p.split(" ")[0], Integer.valueOf(p.split(" ")[1])))
                .toMap(p -> p.getName(), p -> p);


        for (Person p : map.values()) {
            System.out.println(p.getName() + ": " + p.getAge());
        }

    }
}
