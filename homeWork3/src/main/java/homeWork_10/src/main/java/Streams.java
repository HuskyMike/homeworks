package homeWork_10.src.main.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

public class Streams<T> {
    private List<T> someCollection;
    private List<Functional> functionals;


    public Streams(List someCollection) {
        this.someCollection = new ArrayList<>(someCollection);
        functionals = new ArrayList<>();
    }

    public static <E> Streams<E> of(List<E> someCollection) {
        return new Streams<>(someCollection);
    }

    public Streams<T> filter(Predicate<T> predicate) {
        functionals.add(new Functional<T, T>(predicate));
        return this;
    }

    public <R> Streams<R> transform(Function<? super T, ? extends R> function) {
        functionals.add(new Functional<>(function));
        return (Streams<R>) this;
    }

    public <K, V, E> Map<K, V> toMap(Function<T, K> function1, Function<T, V> function2) {
        HashMap<K, V> map = new HashMap<>();
        List<E> listResult = new ArrayList<>();
        for (Functional<E, T> function : functionals) {
            if (function.getPredicate() != null) {
                listResult = new ArrayList<>();
                for (int i = someCollection.size() - 1; i >= 0; i--) {
                    if (function.getPredicate().test((E) someCollection.get(i))) {
                        listResult.add((E) someCollection.get(i));
                    }
                }
            }
            someCollection = (List<T>) listResult;
            if (function.getFunction() != null) {
                for (int i = 0; i < someCollection.size(); i++) {
                    T res = function.getFunction().apply((E) someCollection.get(i));
                    someCollection.set(i, res);
                }
            }
        }

        for (T element : someCollection)
            map.put(function1.apply(element), function2.apply(element));
        return map;
    }

}
