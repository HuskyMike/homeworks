package homeWork_10.src.main.java;

import java.util.function.Function;
import java.util.function.Predicate;

public class Functional<T, E> {

    private Predicate<? super T> predicate;

    private Function<? super T, ? extends E> function;


    public Functional(Predicate<? super T> predicate) {
        this.predicate = predicate;
        function = null;
    }


    public Functional(Function<? super T, ? extends E> function) {
        this.function = function;
        predicate = null;
    }

    public Predicate<? super T> getPredicate() {
        return predicate;
    }

    public Function<? super T, ? extends E> getFunction() {
        return function;
    }

}
