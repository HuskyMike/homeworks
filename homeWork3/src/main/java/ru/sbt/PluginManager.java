package ru.sbt;

public class PluginManager implements Plugin {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
       Class<?> clazz = Class.forName(pluginRootDirectory + "." + pluginClassName + "." + pluginName);
       Plugin plugin = (Plugin) clazz.newInstance();
        return plugin;
    }


    @Override
    public void doUsefull() {
        System.out.println("Work");
    }
}
