package homeWork_13;

import java.util.Stack;

public class MyStack<T> {
    private static final int BUFFER_MAX_SIZE = 10;
    private volatile Stack<T> buffer;

    public MyStack() {
        this.buffer = new Stack<>();
    }

    public synchronized void push(T element) throws InterruptedException {
        while (buffer.size() == BUFFER_MAX_SIZE) {
            wait();
        }
        buffer.push(element);
        notify();
    }

    public synchronized T pop() throws InterruptedException {
        while (buffer.size() == 0) {
            wait();
        }

        T result = buffer.pop();
        notify();
        return result;
    }

    public synchronized boolean empty() {
        return buffer.isEmpty();
    }

    public Stack<T> list() {
        return buffer;
    }
}
