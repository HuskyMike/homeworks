package homeWork_13;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    static volatile int quantity = 1;
    static final Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(1);
        ExecutorService service1 = Executors.newFixedThreadPool(3);
        MyStack<Integer> myStack = new MyStack<>();

        Runnable runnable = () -> {
            try {
                System.out.println(Thread.currentThread().getName() + ": " + myStack.pop());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Runnable runnable1 = () -> {
            synchronized (lock) {
                try {
                    myStack.push(quantity);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                quantity++;
            }
        };


        for (int i = 10; i >= quantity; i--) {
            service1.execute(runnable1);
        }

        service1.shutdown();


        for (int i = 0; i < 10; i++) {
            service.execute(runnable);
        }

        service.shutdown();


    }
}
