package homeWork_11_2.threads;

import homeWork_11_2.interfaces.ThreadPool;

import java.util.LinkedList;
import java.util.Queue;

public class ScalableThreadPool implements ThreadPool {
    private int min;
    private int max;
    private Queue<Runnable> queue = new LinkedList<>();
    private boolean flag = true;

    private Runnable task = () -> {
        synchronized (queue) {
            while (flag == true) {
                if (queue.isEmpty()) {
                    try {
                        System.out.println(Thread.currentThread().getName()
                                + " - ушёл в ожидание.");
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                new Thread(queue.poll()).start();
            }
            System.out.println(Thread.currentThread().getName()
                    + " - закончил.");
        }
    };

    private Runnable taskTime = () -> {
        synchronized (queue) {
            while (!queue.isEmpty()) {
                new Thread(queue.poll()).start();
            }
            System.out.println(Thread.currentThread().getName()
                    + " - закончил.");
        }
    };

    public ScalableThreadPool(int min, int max) {
        this.min = min;
        this.max = max;
    }

    //Запуск потоков
    public void start() {
        synchronized (queue) {
            for (int i = 0; i < min; i++) {
                new Thread(task).start();
            }
        }
    }

    /*Запуск минимального количества поток, изминение на
    максимальное если необходимо
    */
    public void execute(Runnable runnable) {
        queue.add(runnable);
        if (queue.size() <= min) {
            synchronized (queue) {
                queue.notifyAll();
            }
        }
        if (queue.size() > min) {
            for (int i = min; i <= max; i++) {
                new Thread(taskTime).start();
            }
        }
    }


    //Заканчивает работу программы.
    public void end() {
        synchronized (queue) {
            queue.notifyAll();
            flag = false;
        }
    }

}
