package homeWork_11_2.threads;


import homeWork_11_2.interfaces.ThreadPool;

import java.util.LinkedList;
import java.util.Queue;


public class FixedThreadPool implements ThreadPool {
    private final int quantity;
    private Queue<Runnable> queue = new LinkedList<>();
    private boolean flag = true;

    private Runnable task = () -> {
        synchronized (queue) {
            while (flag == true) {
                if (queue.isEmpty()) {
                    try {
                        System.out.println(Thread.currentThread().getName()
                                + " - ушёл в ожидание.");
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                new Thread(queue.poll()).start();
            }
            System.out.println(Thread.currentThread().getName()
                    + " - закончил.");
        }
    };


    public FixedThreadPool(int quantity) {
        this.quantity = quantity;
    }


    //запуск необходимого количества потоков.
    public void start() {
        for (int i = 0; i < quantity; i++) {
            new Thread(task).start();
        }
    }

    public void execute(Runnable runnable) {
        synchronized (queue) {
            queue.add(runnable);
            queue.notifyAll();
        }
    }

    //Заканчивает работу программы.
    public void end() {
        synchronized (queue) {
            queue.notifyAll();
            flag = false;
        }
    }

}
