package homeWork_11_2.interfaces;

public interface ThreadPool {
    void start();

    void execute(Runnable runnable);
}
