package homeWork_11_2;


import homeWork_11_2.threads.FixedThreadPool;
import homeWork_11_2.threads.ScalableThreadPool;

import java.util.concurrent.TimeUnit;


public class Main {
    public static void main(String[] args) throws InterruptedException {
        FixedThreadPool threadPool = new FixedThreadPool(2);
        ScalableThreadPool scalableThreadPool = new ScalableThreadPool(1, 4);

        //FixedThreadPool start
        threadPool.start();
        TimeUnit.SECONDS.sleep(3);
        for (int i = 0; i < 4; i++) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("Задача 1 выполнена");
                }
            });
        }
        TimeUnit.SECONDS.sleep(5);
        threadPool.end();


        //ScalableThreadPool start
        scalableThreadPool.start();
        TimeUnit.SECONDS.sleep(3);
        for (int i = 0; i < 10; i++) {
            scalableThreadPool.execute(() -> {
                System.out.println("Задача 1");
            });
        }
        scalableThreadPool.end();

    }
}
