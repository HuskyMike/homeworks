package homeWork_2;

import java.util.Objects;

public class Credit {
    private String nameCredit;
    private int summCredit;

    @Override
    public String toString() {
        return "{" + "\"сlass\":\"" + Credit.class.getName() + "\"," +
                "\"nameCredit\":\"" + nameCredit + "\"," +
                "\"summCredit\":\"" + summCredit + "\"" +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return summCredit == credit.summCredit &&
                Objects.equals(nameCredit, credit.nameCredit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameCredit, summCredit);
    }

    public Credit(String nameCredit, int summCredit) {
        this.nameCredit = nameCredit;
        this.summCredit = summCredit;
    }

    public String getNameCredit() {
        return nameCredit;
    }

    public void setNameCredit(String nameCredit) {
        this.nameCredit = nameCredit;
    }

    public int getSummCredit() {
        return summCredit;
    }

    public void setSummCredit(int summCredit) {
        this.summCredit = summCredit;
    }
}

