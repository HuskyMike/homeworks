package homeWork_4;

import java.util.*;

public class CountMapMain<T> implements CountMap<T> {
    private final List<T> list = new ArrayList<>();
    private final List<Connection<T>> mapList = new ArrayList<>();


    @Override
    public void add(T element) {
        list.add(element);
    }

    @Override
    public int getCount(T o) {
        int sum = 0;
        for (T element : list) {
            if (o.equals(element)) {
                sum++;
            }
        }
        return sum;
    }

    @Override
    public int remove(T o) {
        int sum = 1;
        for (T element : list) {
            if (o.equals(element)) {
                sum++;
            }
        }
        return sum;
    }

    @Override
    public int size() {
        TreeSet<T> listNotDouble = new TreeSet<>();
        listNotDouble.addAll(list);
        int sum = listNotDouble.size();
        return sum;
    }


    @Override
    public void addAll(CountMap<T> source) {
       Map<T,Integer> map = toMap();
       Integer quantity;
       for (Map.Entry<T,Integer> entry : map.entrySet()){
           for (T obj : list){
               quantity = entry.getValue();
               if (entry.getKey().equals(obj)){
                   quantity++;
               }
               else {
                   list.add(entry.getKey());
               }
           }
       }
    }

    @Override
    public Map<T,Integer> toMap() {
        HashMap<T,Integer> map = new HashMap<>();
        for(Connection<T> connection : mapList){
           map.put(connection.getKey(), connection.getValue());
        }
        return map;
    }

    @Override
    public void toMap(Map<T,Integer> destination) {
        destination.clear();
        destination.putAll(toMap());

    }


}
